// npm init > npm install express > touch .gitignore

// this code will help us access contents of express module/package
	// a "module" is a software component or part of a program that contains 1 or more routines
// it also allows us to access methods and functions that we will use to easily create an app or server
// we store our express module to a variable so we could easily access its keywords, functions, and methods
const express = require("express");

// this code creates an application using express / a.k.a. express application
	// App is our server
const app = express();

// for our application server to run, we need a port to listen to
const port = 3000;



// for our application could read json data
app.use(express.json())

// allows your app to read data from forms
// by default, information received from the url can only be received as string or an array
// by applying the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));


// this route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response) => {

	// this is the response that we will expect to receive if the get method is successful with the right endpoint
	response.send("GET method success. \nHello from /hello endpoint!");
});

app.post("/hello", (request, response) => {

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== "") {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered`);

		// only the first response will display
		// response.send('Welcome admin');
	}
	else {
		response.send(`Please input BOTH username and password`)
	}
	console.log(users);
});

// Syntax
/*
app.httpMethod("/endpoint", (request, response) =>{

	//code block

});
*/

// we use PUT Method to update our document
app.put("/change-password", (request, response) => {

	// the purpose of our loop is to check our per element in users array if it match our request.body.username
	// we create a variable where we will store our response
	let message;

	for(let i = 0; i < users.length; i++) {
		if(request.body.username == users[i].username) {

			// update an element object's password based on the input on the body
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated`
			break;
		}
		else {
			message = "User does not exist."
		}
	}

	// we display our updated array
	console.log(users)

	// we display our response based if there is a match (successfully updated the password) or if there is no match (user does not exist)
	response.send(message);

})

// tells our server/application to listen to the port
// if the port is a accessed, we can run the server
// returns a message to confirm that the server is running in the terminal
// app.listen(port,()=> console.log(`Server running at port ${port}`));

// 1
// Create a route that expects GET request at the URI/endpoint "/home"
// response that will be received should be "Welcome to the homepage"
// code below...

app.get("/home", (request, response) =>{
	response.send('Welcome to the homepage')
});

// 2
// create a route that expects GET request at the URI/endpoint "/users"
// response that will be received is a collection of users
// code below...

app.get("/users", (request, response) =>{
	response.send(users)
});

// 3
// create a route that expects DELETE request at the URI/endpoint "/delete-user"
// the program should check if the user is existing before deleting
// response that will be received is the updated collection of users
// code below...

app.delete("/delete-user", (request, response) =>{

	let message;

	for(let i = 0; i < users.length; i++) {
		if(request.body.username === users[i].username){
			users.splice(i, 1);
			message = `${request.body.username} has been deleted`
			break;
		}
		else {
			message = "User does not exist."
		}
	}

	response.send(users);
	console.log(message);
	console.log(users);
})

app.listen(port,()=> console.log(`Server running at port ${port}`));




// 4 
// save all the successful request tabs and export the collection
// update your GitLab repo and link it to Boodle